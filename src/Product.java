import java.util.Objects;

public class Product {
    private int ProductId;
    private String ProductName;

    private static final int defaultID = -1;

    public Product(String productName) {
        this.ProductId = defaultID;
        this.ProductName = productName;
    }

    public Product(Integer productID, String productName) {
        this.ProductId = productID;
        this.ProductName = productName;
    }

    public int getProductID() {
        return ProductId;
    }

    public void setProductID(int productID) {
        this.ProductId = productID;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        this.ProductName = productName;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return ProductId == product.ProductId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(ProductId);
    }
}
