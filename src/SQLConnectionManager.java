
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SQLConnectionManager {


    public Connection openSQLConnection() {
        Connection con = null;
        try {
            con = DriverManager.getConnection("jdbc:sqlite:/Users/atereh/Product.db");
            System.out.println("OPEN CONNECTION\t");
        } catch (SQLException e) {
            System.out.println("COULDN'T OPEN A NEW CONNECTION:\t" + e.getLocalizedMessage());
        }
        return con;
    }

    public void closeSQLConnection(Connection con) {
        try {
            con.close();
        } catch (SQLException e) {
            System.out.println("COULDN'T CLOSE A CONNECTION:\t" + e.getLocalizedMessage());
        }
    }
}
