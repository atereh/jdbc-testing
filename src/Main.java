import java.sql.*;
import java.util.List;

public class Main {


    public static void main(String[] args) {

        SQLConnectionManager sql = new SQLConnectionManager();

        ProductDAO productDAO = new ProductDAOSQLImpl(sql);

        Product newProd = new Product("test");

        List<Product> allProducts = productDAO.getAllProducts();
        for (Product product: allProducts) {
            System.out.println(product.getProductID() + "\t" + product.getProductName());
        }



    }
}


