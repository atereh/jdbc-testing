import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("ALL")
public class ProductDAOSQLImpl implements ProductDAO {

    private SQLConnectionManager sqlConnectionManager;

    public ProductDAOSQLImpl(SQLConnectionManager sqlConnectionManager) {
        this.sqlConnectionManager = sqlConnectionManager;
    }

    @Override
    public List<Product> getAllProducts() {
        List<Product> productsList = new ArrayList<>();



        String sql = "SELECT * FROM products";



        Connection con = sqlConnectionManager.openSQLConnection();

        try {
            Statement statement = con.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                Integer id = resultSet.getInt("ProductId");
                String name = resultSet.getString("ProductName");
                System.out.printf("%d  %s \n", id, name);


            }
        } catch (SQLException e) {
            System.out.println("FAILED TO GET ALL PRODUCTS:\t" + e.getLocalizedMessage());
        } finally {
            sqlConnectionManager.closeSQLConnection(con);
        }
        return productsList;
    }

    @Override
    public void addProduct(Product product) {
        String sql = "INSERT INTO products VALUES (?)";

        Connection con = sqlConnectionManager.openSQLConnection();

        try {
            PreparedStatement preparedStatement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, product.getProductName());

            preparedStatement.executeUpdate();

            // get last ProductID of the last inserted item
            ResultSet generatedID = preparedStatement.getGeneratedKeys();
            if (generatedID.next()) {
                Integer newID = generatedID.getInt(1);
                product.setProductID(newID);
            } else {
                throw new SQLException("Creating Product failed, no ID obtained.");
            }
        } catch (SQLException e) {
            System.out.println("FAILED TO ADD A NEW PRODUCT:\t" + e.getLocalizedMessage());
        } finally {
            sqlConnectionManager.closeSQLConnection(con);
        }
    }

    @Override
    public void updateProduct(Product product) { //to be implemented may be

    }

    @Override
    public void deleteProduct(Product product) { //to be implemented may be

    }
}
