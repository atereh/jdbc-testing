import java.util.List;

public interface ProductDAO {
        List<Product> getAllProducts();
        void addProduct(Product product);
        void updateProduct(Product product);
        void deleteProduct(Product product);
}
